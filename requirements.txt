tensorflow_gpu==1.1.0

# Last TF-gpu is blocking as "`tf.add_check_numerics_ops() is not compatible with TensorFlow control
#   flow operations such as `tf.cond()` or `tf.while_loop()`"

scipy==0.19.0
scikit_image==0.13.0
matplotlib==2.0.1
numpy==1.12.1
Pillow==4.1.1
protobuf==3.3.0
scikit_learn==0.18.1
tensorflow==1.1.0
opencv-python